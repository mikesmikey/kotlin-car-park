package com.example.carpark

data class CarInfo(var plateNumber: String = "", var carBrand: String = "", var ownerName: String = "")