package com.example.carpark


import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.carpark.databinding.FragmentLoginBinding
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(
            inflater,
            R.layout.fragment_login, container, false
        )
        binding.loginButton.setOnClickListener {
            login(it, binding)
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun login (view: View, binding: FragmentLoginBinding) {
        if (!binding.usernameInput.text.toString().equals("jankob")) {
            errorAlert(binding, "username ไม่ถูกต้อง")
        } else if (!binding.passwordInput.text.toString().equals("jankob")) {
            errorAlert(binding, "password ไม่ถูกต้อง")
        } else {
            errorAlert(binding, "")
            view.findNavController()
                .navigate(R.id.action_loginFragment_to_carPlateFragment)
        }
    }

    private fun errorAlert(binding: FragmentLoginBinding, text: String) {
        if (text != "") {
            binding.loginErrorText.text = text
            binding.loginErrorText.visibility = View.VISIBLE
        } else {
            binding.loginErrorText.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
