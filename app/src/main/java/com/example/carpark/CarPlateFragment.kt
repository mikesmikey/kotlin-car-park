package com.example.carpark


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.inflate
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.carpark.databinding.FragmentCarPlateBinding
import kotlinx.android.synthetic.main.fragment_car_plate.*

/**
 * A simple [Fragment] subclass.
 */
class CarPlateFragment : Fragment() {

    private val carInfo1: CarInfo = CarInfo("Slot 1", "", "")
    private val carInfo2: CarInfo = CarInfo("Slot 2", "", "")
    private val carInfo3: CarInfo = CarInfo("Slot 3", "", "")
    private var currentSlot: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = inflate<FragmentCarPlateBinding>(
            inflater,
            R.layout.fragment_car_plate, container, false
        )
        updateViewSlot(binding)
        binding.apply {
            slotTextView1.setOnClickListener {
                loadSlot(1, binding)
            }
            slotTextView2.setOnClickListener {
                loadSlot(2, binding)
            }
            slotTextView3.setOnClickListener {
                loadSlot(3, binding)
            }
            updateSlotButton.setOnClickListener {
                updateSlot(currentSlot, binding)
            }
            deleteSlotButton.setOnClickListener {
                deleteSlot(currentSlot, binding)
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun loadSlot(number: Int, binding: FragmentCarPlateBinding) {
        binding.carInfoText.text = "Car Info: Slot " + number
        currentSlot = number

        var data = when (number) {
            1 -> carInfo1
            2 -> carInfo2
            3 -> carInfo3
            else -> carInfo1
        }

        binding.apply {
            plateNumberInput.setText(data.plateNumber)
            carBrandInput.setText(data.carBrand)
            ownerNameInput.setText(data.ownerName)
        }
    }

    private fun updateSlot(number: Int, binding: FragmentCarPlateBinding) {

        var data = when (number) {
            1 -> carInfo1
            2 -> carInfo2
            3 -> carInfo3
            else -> carInfo1
        }

        data.plateNumber = binding.plateNumberInput.text.toString()
        data.carBrand = binding.carBrandInput.text.toString()
        data.ownerName = binding.ownerNameInput.text.toString()
        updateViewSlot(binding)
    }

    private fun updateViewSlot(binding: FragmentCarPlateBinding) {
        binding.carInfo1 = carInfo1
        binding.carInfo2 = carInfo2
        binding.carInfo3 = carInfo3
        loadSlot(currentSlot, binding)
    }

    private fun deleteSlot(number: Int, binding: FragmentCarPlateBinding) {
        var data = when (number) {
            1 -> carInfo1
            2 -> carInfo2
            3 -> carInfo3
            else -> carInfo1
        }

        data.plateNumber = "Slot " + number
        data.carBrand = ""
        data.ownerName = ""
        updateViewSlot(binding)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
